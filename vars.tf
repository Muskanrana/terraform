variable "vpc_cidr" {
  default = "10.0.0.0/16"
}
variable "public_subnet" {
  type = list
  default = [ "10.0.0.0/24" , "10.0.1.0/24" ]
}

variable "private_subnet" {
 type = list
  default = [ "10.0.2.0/24" , "10.0.3.0/24" ]
}

variable "vpc_name" {
  default = "ninja-vpc-01"
}
variable "IGW_name" {
  default = "ninja-igw-01"
}

variable "NAT_name" {
  default = "ninja-nat-01"
}
variable "PUB_name" {
  type = list
  default = [ "ninja-pub-01" , "ninja-pub-02"]
}
variable "PRIV_name" {
  type = list
  default = [ "ninja-priv-01" , "ninja-priv-02"]
}

variable "Pub_route" {
  default = "ninja-pub-rt-01"
}
variable "Priv_route" {
  default = "ninja-priv-rt-01"
}

variable "aws_az" {
   type = list
   default = [ "ap-south-1a" , "ap-south-1b" ]
}
variable "aws_az1" {
  type = list
  default = [ "ap-south-1a" , "ap-south-1b" ]
}
variable "ami" {
 default = "ami-0860c9429baba6ad2"
}

variable "ins_type" {
 default = "t2.micro"
}

variable "ec2_Instance_pub" {
 type = list
 default = [ "ec2-pub-01" , "ec2-pub-02" ]
}

variable "ec2_Instance_priv" {
 type = list
 default = [ "ec2-priv-01" , "ec2-priv-02" ]
}
variable "destination_cidr_block" {
 default = "0.0.0.0/0"
}
variable "My_Ip" {
 default = "0.0.0.0/0"
}
variable "bastion_private_ip" {
 default = "0.0.0.0/0"
}
variable "public_Subnet_SG" {
 default = "public-sg-01"
}
variable "private_Subnet_SG" {
 default = "private-sg-01"
}

