#Creating the VPC
resource "aws_vpc" "Main" {
  cidr_block = var.vpc_cidr
  tags = {
    Name = var.vpc_name
  }
}

#Creating Internet Gateway and attach it to VPC
resource "aws_internet_gateway" "IGW" {
  vpc_id = aws_vpc.Main.id

  tags = {

    Name = var.IGW_name
  }
}


#Creating the NAT Gateway using subnet_id and allocation_id
resource "aws_nat_gateway" "NATgw" {
  allocation_id = aws_eip.natIP.id
  subnet_id   =  aws_subnet.public_subnet[0].id
  tags = {
    Name = var.NAT_name
  }
}


#Allocating IP to NAT
resource "aws_eip" "natIP" {
  vpc        = true
  depends_on = [aws_internet_gateway.IGW]
}


#Creating  a Public Subnets.
resource "aws_subnet" "public_subnet" {
  count =  length(var.public_subnet)
  vpc_id     = aws_vpc.Main.id
  cidr_block = var.public_subnet[count.index]
  map_public_ip_on_launch = true
  availability_zone = var.aws_az[count.index]

tags = {
    Name = var.PUB_name[count.index]
  }
}


#Create a Private Subnet1
resource "aws_subnet" "private_subnet" {
  count = length(var.private_subnet)
  vpc_id  = aws_vpc.Main.id
  cidr_block = var.private_subnet[count.index]
  availability_zone  = var.aws_az1[count.index]

  tags = {
    Name = var.PRIV_name[count.index]
  }
}


#Route table for Public Subnet's
resource "aws_route_table" "public_route" {
  vpc_id = "${aws_vpc.Main.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.IGW.id}"

  }
  tags = {
    Name = var.Pub_route
  }
}


# Route table for Private Subnet's
resource "aws_route_table" "private_route" {
  vpc_id = "${aws_vpc.Main.id}"
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.NATgw.id
  }
  tags = {
    Name = var.Priv_route
  }
}

resource "aws_route_table_association" "public_subnet_assoc" {
  count = "${length(var.public_subnet)}"
  route_table_id = aws_route_table.public_route.id
  subnet_id = element(aws_subnet.public_subnet.*.id,count.index)

}

 resource "aws_route_table_association" "private_subnet_assoc" {
  count =  "${length(var.private_subnet)}"
  route_table_id = "${aws_route_table.private_route.id}"
  subnet_id = element(aws_subnet.private_subnet.*.id,count.index)

}


resource "aws_instance" "ec2_private" {
   count = "${length(var.private_subnet)}"
   ami = var.ami
  instance_type = var.ins_type
   key_name =  "muskan"
  vpc_security_group_ids = [aws_security_group.privateSubnet.id]
   subnet_id = element(aws_subnet.private_subnet.*.id,count.index)
   tags = {
    Name = var.ec2_Instance_priv[count.index]
  }
}

resource "aws_instance" "ec2_public" {
   count = "${length(var.public_subnet)}"
   ami = var.ami
  instance_type = var.ins_type
   key_name =  "muskan"
  vpc_security_group_ids = [aws_security_group.publicSubnet.id]
   subnet_id = element(aws_subnet.public_subnet.*.id,count.index)
   tags = {
    Name = var.ec2_Instance_pub[count.index]
  }
}


resource "aws_security_group" "publicSubnet" {
   vpc_id = aws_vpc.Main.id
   ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.My_Ip]
  }

egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.destination_cidr_block]
  }
    tags = {

    Name = var.public_Subnet_SG
  }
}

#Private SG

resource "aws_security_group" "privateSubnet" {
   vpc_id = aws_vpc.Main.id
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.bastion_private_ip]
  }

egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.destination_cidr_block]
  }
    tags = {

    Name = var.private_Subnet_SG

}


}


